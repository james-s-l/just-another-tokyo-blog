# Simple Blog

### Getting Started

#### Run in development
```
yarn run development
```

#### Run in production
```
COSMIC_BUCKET=your-bucket-slug yarn start
```
Open [http://localhost:3000](http://localhost:3000).

### Node.js + Cosmic JS

1. [Log in to Cosmic JS](https://cosmicjs.com).
2. Create a Bucket.
3. Go to Your Bucket > Apps.
4. Install the [Simple Blog Website](https://cosmicjs.com/apps/simple-blog).
5. Deploy your Blog Roll to the Cosmic App Server at Your Bucket > Web Hosting.
